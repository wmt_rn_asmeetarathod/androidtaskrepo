package com.example.androidtask;

import com.google.gson.annotations.SerializedName;

public class NotificationsItem{

	@SerializedName("notification_type")
	private int notificationType;

	@SerializedName("is_read")
	private int isRead;

	@SerializedName("notificational_id")
	private Object notificationalId;

	@SerializedName("user_id")
	private Object userId;

	@SerializedName("description")
	private String description;

	@SerializedName("created_at")
	private int createdAt;

	@SerializedName("notificational_type")
	private Object notificationalType;

	@SerializedName("id")
	private int id;

	@SerializedName("products")
	private Object products;

	@SerializedName("challenges")
	private Challenges challenges;

	public void setNotificationType(int notificationType){
		this.notificationType = notificationType;
	}

	public int getNotificationType(){
		return notificationType;
	}

	public void setIsRead(int isRead){
		this.isRead = isRead;
	}

	public int getIsRead(){
		return isRead;
	}

	public void setNotificationalId(Object notificationalId){
		this.notificationalId = notificationalId;
	}

	public Object getNotificationalId(){
		return notificationalId;
	}

	public void setUserId(Object userId){
		this.userId = userId;
	}

	public Object getUserId(){
		return userId;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(int createdAt){
		this.createdAt = createdAt;
	}

	public int getCreatedAt(){
		return createdAt;
	}

	public void setNotificationalType(Object notificationalType){
		this.notificationalType = notificationalType;
	}

	public Object getNotificationalType(){
		return notificationalType;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProducts(Object products){
		this.products = products;
	}

	public Object getProducts(){
		return products;
	}

	public void setChallenges(Challenges challenges){
		this.challenges = challenges;
	}

	public Challenges getChallenges(){
		return challenges;
	}
}