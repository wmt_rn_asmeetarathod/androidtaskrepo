package com.example.androidtask;

import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("pagination")
	private Pagination pagination;

	@SerializedName("data")
	private Data data;

	@SerializedName("meta")
	private Meta meta;

	public void setPagination(Pagination pagination){
		this.pagination = pagination;
	}

	public Pagination getPagination(){
		return pagination;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setMeta(Meta meta){
		this.meta = meta;
	}

	public Meta getMeta(){
		return meta;
	}
}