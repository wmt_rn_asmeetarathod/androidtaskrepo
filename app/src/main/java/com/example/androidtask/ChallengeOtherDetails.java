package com.example.androidtask;

import com.google.gson.annotations.SerializedName;

public class ChallengeOtherDetails{

	@SerializedName("complete_step")
	private int completeStep;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("challenge_id")
	private int challengeId;

	@SerializedName("finish_date_time")
	private int finishDateTime;

	@SerializedName("challenge_status")
	private int challengeStatus;

	@SerializedName("id")
	private int id;

	@SerializedName("user_rank")
	private int userRank;

	@SerializedName("user")
	private User user;

	public void setCompleteStep(int completeStep){
		this.completeStep = completeStep;
	}

	public int getCompleteStep(){
		return completeStep;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setChallengeId(int challengeId){
		this.challengeId = challengeId;
	}

	public int getChallengeId(){
		return challengeId;
	}

	public void setFinishDateTime(int finishDateTime){
		this.finishDateTime = finishDateTime;
	}

	public int getFinishDateTime(){
		return finishDateTime;
	}

	public void setChallengeStatus(int challengeStatus){
		this.challengeStatus = challengeStatus;
	}

	public int getChallengeStatus(){
		return challengeStatus;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUserRank(int userRank){
		this.userRank = userRank;
	}

	public int getUserRank(){
		return userRank;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}
}