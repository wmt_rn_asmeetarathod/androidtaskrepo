package com.example.androidtask;

import android.content.Context;

public class DataClassSignUp {
    String firstName,lastName,contactNo,sEmail,sPassword,sConfirmPassword,userId;
    Context context;
    public DataClassSignUp(){}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public DataClassSignUp(String firstName, String lastName, String contactNo, String sEmail) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNo = contactNo;
        this.sEmail = sEmail;
    }

    public DataClassSignUp(Context context,String firstName, String lastName, String contactNo, String sEmail, String userId) {
        this.context=context;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNo = contactNo;
        this.sEmail = sEmail;
        this.userId = userId;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public DataClassSignUp(String firstName, String lastName, String contactNo, String sEmail, String sPassword, String sConfirmPassword) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNo = contactNo;
        this.sEmail = sEmail;
        this.sPassword = sPassword;
        this.sConfirmPassword = sConfirmPassword;
    }

    public DataClassSignUp(String firstName, String lastName, String contactNo, String sEmail, String sPassword, Context context) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNo = contactNo;
        this.sEmail = sEmail;
        this.sPassword = sPassword;
        this.context = context;
    }

    public DataClassSignUp(String firstName, String lastName, String contactNo, String sEmail, String sPassword) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNo = contactNo;
        this.sEmail = sEmail;
        this.sPassword = sPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getsEmail() {
        return sEmail;
    }

    public void setsEmail(String sEmail) {
        this.sEmail = sEmail;
    }

    public String getsPassword() {
        return sPassword;
    }

    public void setsPassword(String sPassword) {
        this.sPassword = sPassword;
    }

    public String getsConfirmPassword() {
        return sConfirmPassword;
    }

    public void setsConfirmPassword(String sConfirmPassword) {
        this.sConfirmPassword = sConfirmPassword;
    }
}
