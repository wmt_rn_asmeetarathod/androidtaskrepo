package com.example.androidtask;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomViewHolder> {


    ArrayList<Data> arrayList=new ArrayList<>();

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.notificationlayout,parent,false);

        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class CustomViewHolder extends RecyclerView.ViewHolder{

        public TextView tvDesc,tvCreatedAt,tvTime;


        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDesc=itemView.findViewById(R.id.tvDesc);
            tvCreatedAt=itemView.findViewById(R.id.tvCreated);
            tvTime=itemView.findViewById(R.id.tvTime);

        }
    }
}
