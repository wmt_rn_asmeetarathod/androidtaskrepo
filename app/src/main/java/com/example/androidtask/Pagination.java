package com.example.androidtask;

import com.google.gson.annotations.SerializedName;

public class Pagination{

	@SerializedName("total")
	private int total;

	@SerializedName("next_page_url")
	private String nextPageUrl;

	@SerializedName("previous_page_url")
	private Object previousPageUrl;

	@SerializedName("current_page")
	private int currentPage;

	public void setTotal(int total){
		this.total = total;
	}

	public int getTotal(){
		return total;
	}

	public void setNextPageUrl(String nextPageUrl){
		this.nextPageUrl = nextPageUrl;
	}

	public String getNextPageUrl(){
		return nextPageUrl;
	}

	public void setPreviousPageUrl(Object previousPageUrl){
		this.previousPageUrl = previousPageUrl;
	}

	public Object getPreviousPageUrl(){
		return previousPageUrl;
	}

	public void setCurrentPage(int currentPage){
		this.currentPage = currentPage;
	}

	public int getCurrentPage(){
		return currentPage;
	}
}