package com.example.androidtask;

import com.google.gson.annotations.SerializedName;

public class Vendor{

	@SerializedName("birthdate")
	private String birthdate;

	@SerializedName("address")
	private String address;

	@SerializedName("gender")
	private int gender;

	@SerializedName("mobile_no")
	private String mobileNo;

	@SerializedName("admin")
	private Admin admin;

	@SerializedName("profile_picture")
	private String profilePicture;

	@SerializedName("uuid")
	private String uuid;

	@SerializedName("is_verified")
	private int isVerified;

	@SerializedName("is_admin")
	private int isAdmin;

	@SerializedName("country_code")
	private Object countryCode;

	@SerializedName("vendor_type_id")
	private int vendorTypeId;

	@SerializedName("admin_id")
	private int adminId;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("remember_token")
	private Object rememberToken;

	@SerializedName("email")
	private String email;

	public void setBirthdate(String birthdate){
		this.birthdate = birthdate;
	}

	public String getBirthdate(){
		return birthdate;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setGender(int gender){
		this.gender = gender;
	}

	public int getGender(){
		return gender;
	}

	public void setMobileNo(String mobileNo){
		this.mobileNo = mobileNo;
	}

	public String getMobileNo(){
		return mobileNo;
	}

	public void setAdmin(Admin admin){
		this.admin = admin;
	}

	public Admin getAdmin(){
		return admin;
	}

	public void setProfilePicture(String profilePicture){
		this.profilePicture = profilePicture;
	}

	public String getProfilePicture(){
		return profilePicture;
	}

	public void setUuid(String uuid){
		this.uuid = uuid;
	}

	public String getUuid(){
		return uuid;
	}

	public void setIsVerified(int isVerified){
		this.isVerified = isVerified;
	}

	public int getIsVerified(){
		return isVerified;
	}

	public void setIsAdmin(int isAdmin){
		this.isAdmin = isAdmin;
	}

	public int getIsAdmin(){
		return isAdmin;
	}

	public void setCountryCode(Object countryCode){
		this.countryCode = countryCode;
	}

	public Object getCountryCode(){
		return countryCode;
	}

	public void setVendorTypeId(int vendorTypeId){
		this.vendorTypeId = vendorTypeId;
	}

	public int getVendorTypeId(){
		return vendorTypeId;
	}

	public void setAdminId(int adminId){
		this.adminId = adminId;
	}

	public int getAdminId(){
		return adminId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setRememberToken(Object rememberToken){
		this.rememberToken = rememberToken;
	}

	public Object getRememberToken(){
		return rememberToken;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}
}