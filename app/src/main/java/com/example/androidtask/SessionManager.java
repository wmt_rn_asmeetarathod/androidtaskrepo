package com.example.androidtask;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.HashMap;


public class SessionManager extends AppCompatActivity {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;

    int PRIVATE_MODE=0;

    private static final String prefName="SessionPref";
    private static final String IS_LOGIN="IsLoggedIn";
    public static final String keyName="username";
    public static final String keyToken="fcm_token";
    public static final String keyId="id";


    public SessionManager(Context context) {
        this.context = context;
        pref=context.getSharedPreferences(prefName,PRIVATE_MODE);
        editor=pref.edit();
    }

    public void createLoginSession(String token,String mailId){
        editor.putBoolean(IS_LOGIN,true);
        //editor.putString(keyId,userId);
        editor.putString(keyToken,token);
        editor.putString(keyName,mailId);

        editor.commit();
    }

    public void checkLogin(){
        if(!this.isLoggedIn()){
            LoginFragment loginFragment=new LoginFragment();
            FragmentManager fragmentManager=getSupportFragmentManager();
            FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment1,loginFragment);
            fragmentTransaction.hide(new ProfileFragment());
            fragmentTransaction.hide(new SignUpFragment());
            fragmentTransaction.commit();

        };
    }

    public HashMap<String,String> getUserDetails(){
        HashMap<String,String> user=new HashMap<String,String>();
        //user.put(keyId,pref.getString(keyId,null));
        user.put(keyToken,pref.getString(keyToken,null));
        user.put(keyName,pref.getString(keyName,null));
        return user;
    }

    public void logOutUser(){
        editor.clear();
        editor.commit();
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN,false);
    }
}
