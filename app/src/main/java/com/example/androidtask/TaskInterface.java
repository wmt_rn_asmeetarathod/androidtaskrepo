package com.example.androidtask;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TaskInterface {

    @POST("/api/v1/signin")
    @FormUrlEncoded
    Call<User> signInData(@Field("email") String mailId,
                                     @Field("password") String password,
                                     @Field("fcm_token") String token,
                                     @Field("device_type") String deviceType,
                                     @Field("device_name") String deviceName,
                                     @Field("os") String os);


    @POST("/api/v1/get_notification?page=1")
    @FormUrlEncoded
    Call<Response> getNotificationData( @Field("fcm_token") String token);
    //@Query("page") String page,
}
