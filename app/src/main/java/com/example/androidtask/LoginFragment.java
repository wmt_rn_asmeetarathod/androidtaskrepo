package com.example.androidtask;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends Fragment implements View.OnClickListener, LoginView {

    SharedPreferences sp;
    EditText edUserName, edlPassword;
    String sUserName, sPassword;

    Button btLogin;
    LoginPresenter presenter;
    String res;
    AwesomeValidation awesomeValidation;
    TextView tvForgotPassword;

    public LoginFragment() {

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edUserName = view.findViewById(R.id.edtUserName);
        edlPassword = view.findViewById(R.id.edtLPassword);

        btLogin = view.findViewById(R.id.btnLogin);
        tvForgotPassword = view.findViewById(R.id.tvForgotPassword);

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assert getFragmentManager() != null;
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Fragment fr = getFragmentManager().findFragmentByTag("forgotPasswordDialog");
                if (fr != null) {
                    transaction.remove(fr);
                }
                transaction.addToBackStack(null);

                DialogFragment dialogFragment = new ForgotPasswordFragment();
                dialogFragment.show(transaction, "forgotPasswordDialog");

            }
        });

        presenter = new LoginPresenter(this);
        btLogin.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        return view;
    }

    @Override
    public void onClick(View v) {

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        awesomeValidation.addValidation(getActivity(), R.id.edtUserName, Patterns.EMAIL_ADDRESS, R.string.emailError);
        awesomeValidation.addValidation(getActivity(), R.id.edtLPassword, "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$", R.string.pswdError);

        if (awesomeValidation.validate()) {

            SessionManager session = new SessionManager(v.getContext());
            sUserName = edUserName.getText().toString();
            sPassword = edlPassword.getText().toString();
            //  res = presenter.checkUser(sUserName, sPassword, v.getContext());
            //if (res.equals("0")) {

            //  Toast.makeText(getContext(), "Invalid Username or Password!", Toast.LENGTH_LONG).show();

            //} else {

            TaskInterface taskInterface = RetroInstance.getInstance().create(TaskInterface.class);
            Call<com.example.androidtask.User> call = taskInterface.signInData(sUserName, sPassword, "efsdsgfrfhdb", "android", "nexus 5", "android");

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        session.createLoginSession("efsdsgfrfhdb", sUserName);
                        Toast.makeText(getContext(), "Login Successfully", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(getActivity(), NotificationProfileActivity.class);
                        startActivity(i);

                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {

                }
            });


            //   }
        }

    }

    public void callProfileFragment() {
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putString("userLoginId", res);
        profileFragment.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment1, profileFragment);
        fragmentTransaction.hide(this);
        fragmentTransaction.hide(new SignUpFragment());
        fragmentTransaction.commit();
    }


    @Override
    public String checkUser(String s1, String s2, Context context) {
        return "0";
    }


}