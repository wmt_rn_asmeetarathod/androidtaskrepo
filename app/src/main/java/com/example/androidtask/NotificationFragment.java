package com.example.androidtask;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

public class NotificationFragment extends Fragment {


    String uToken, uName;
    ImageButton ibProfile;

    public NotificationFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        SessionManager session = new SessionManager(getContext());
        HashMap<String, String> user = session.getUserDetails();
        uToken = user.get(SessionManager.keyToken);
        uName = user.get(SessionManager.keyName);

        TaskInterface taskInterface = RetroInstance.getInstance().create(TaskInterface.class);
        Call<com.example.androidtask.Response> call = taskInterface.getNotificationData( uToken);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });


        return view;


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ibProfile = view.findViewById(R.id.ibProfile);


        ibProfile.setOnClickListener(v -> {
            FragmentManager manager = getFragmentManager();
            assert manager != null;
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.notificationProfileFragment, new ProfileFragment());
            transaction.hide(new LoginFragment());
            transaction.hide(new NotificationFragment());
            transaction.hide(new SignUpFragment());

            transaction.addToBackStack(null);
            transaction.commit();

        });
    }
}