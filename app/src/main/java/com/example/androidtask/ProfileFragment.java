package com.example.androidtask;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import java.util.Objects;


public class ProfileFragment extends Fragment implements com.example.androidtask.ProfileView, View.OnClickListener {


    AwesomeValidation awesomeValidation;
    Button btLogout, btUpdateProfile;
    String userId;
    String fName, lName, contact, emailId;
    com.example.androidtask.ProfilePresenter profilePresenter;
    EditText tvfName, tvlName, tvEmail, tvContact;
    ImageView ivProfile;
    public static final int CAMERA_PERMISSION_CODE = 1;


    public ProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        // assert getArguments() != null;
        //   userId = getArguments().getString("userLoginId");
        //    Log.e("userId in Profile", userId);
        //profilePresenter = new com.example.androidtask.ProfilePresenter(this);
       /* DataClassSignUp dc = profilePresenter.getUserData(userId, getContext());


        tvfName = view.findViewById(R.id.tvfName);
        tvlName = view.findViewById(R.id.tvlName);
        tvContact = view.findViewById(R.id.tvContact);
        tvEmail = view.findViewById(R.id.tvEmail);
        ivProfile = view.findViewById(R.id.ivProfile);

        tvfName.setText(dc.getFirstName());
        tvlName.setText(dc.getLastName());
        tvContact.setText(dc.getContactNo());
        tvEmail.setText(dc.getsEmail());
*/
        btLogout = view.findViewById(R.id.btnLogOut);
        btLogout.setOnClickListener(this);


        ivProfile.setOnClickListener(v -> {
            selectImage();
        });

/*
        btUpdateProfile = view.findViewById(R.id.btnUpdateProfile);
        btUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);


                awesomeValidation.addValidation(getActivity(), R.id.tvfName, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameError);
                awesomeValidation.addValidation(getActivity(), R.id.tvlName, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameError);
                awesomeValidation.addValidation(getActivity(), R.id.tvContact, "^[0-9]{2}[0-9]{8}$", R.string.contactError);
                awesomeValidation.addValidation(getActivity(), R.id.tvEmail, Patterns.EMAIL_ADDRESS, R.string.emailError);

                if (awesomeValidation.validate()) {


                    fName = tvfName.getText().toString();
                    lName = tvlName.getText().toString();
                    contact = tvContact.getText().toString();
                    emailId = tvEmail.getText().toString();
                    int res = 0;
                    res = profilePresenter.updateData(new DataClassSignUp(v.getContext(), fName, lName, contact, emailId, userId));
                    if (res == 1) {
                        Toast.makeText(getContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), "Try Again!", Toast.LENGTH_LONG).show();
                    }
                }


            }
        });*/

        return view;
    }


    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallary", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setTitle("Add Photo!");

        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);

            } else if (options[item].equals("Choose from Gallary")) {
                Intent gallaryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallaryIntent, 2);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    @Override
    public DataClassSignUp getUserData(String userId, Context context) {

        return null;
    }

    @Override
    public int updateData(DataClassSignUp dc) {
        return 0;
    }

    @Override
    public void onClick(View v) {
        callLoginFragment();
        com.example.androidtask.SessionManager session = new com.example.androidtask.SessionManager(v.getContext());
        session.logOutUser();

    }

    public void callLoginFragment() {
        LoginFragment loginFragment = new LoginFragment();

        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment1, loginFragment);
        fragmentTransaction.hide(this);
        fragmentTransaction.hide(new SignUpFragment());
        fragmentTransaction.commit();
    }

    public void checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), permission) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new String[]{permission}, requestCode);
        } else {
            Toast.makeText(getContext(), "Permission already Granted", Toast.LENGTH_LONG).show();
            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(i, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Camera Permission Granted", Toast.LENGTH_LONG).show();
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i, 1);
            } else {
                Toast.makeText(getContext(), "Camera Permission Denied", Toast.LENGTH_LONG).show();
                showSettingsDialog();

            }
        }
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Need Permissions");
        builder.setMessage("This App needs permission to use this feature. You can grant them in App Settings");
        builder.setPositiveButton("GOTO Settings", ((dialog, which) -> {
            dialog.cancel();
            openSettings();
        }));
        builder.setNegativeButton("Cancel", ((dialog, which) -> {
            dialog.cancel();
        }));
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", "com.example.demoproject", null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            ivProfile.setImageBitmap(photo);
        } else if (requestCode == 2) {
            Uri selImage = data.getData();
            ivProfile.setImageURI(selImage);
        }
    }


}