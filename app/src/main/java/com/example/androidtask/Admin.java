package com.example.androidtask;

import com.google.gson.annotations.SerializedName;

public class Admin{

	@SerializedName("name")
	private String name;

	@SerializedName("profile_picture")
	private Object profilePicture;

	@SerializedName("id")
	private int id;

	@SerializedName("uuid")
	private String uuid;

	@SerializedName("email")
	private String email;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setProfilePicture(Object profilePicture){
		this.profilePicture = profilePicture;
	}

	public Object getProfilePicture(){
		return profilePicture;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUuid(String uuid){
		this.uuid = uuid;
	}

	public String getUuid(){
		return uuid;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}
}