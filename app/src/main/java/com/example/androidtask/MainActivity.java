package com.example.androidtask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Fragment> fragmentList = new ArrayList<>();
    SessionManager session;
    String uId, uName;
    TextView tvSignIn, tvSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvSignIn = findViewById(R.id.tvMSignIn);
        tvSignUp = findViewById(R.id.tvMSignUp);

        LoginFragment loginFragment = new LoginFragment();
        SignUpFragment signUpFragment = new SignUpFragment();
        ProfileFragment profileFragment = new ProfileFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.add(loginFragment, "LoginFragment");

        session = new SessionManager(getApplicationContext());
        Toast.makeText(getApplicationContext(), "User Login Status : " + session.isLoggedIn(), Toast.LENGTH_LONG).show();

     /*   if (session.isLoggedIn()) {
            HashMap<String, String> user = session.getUserDetails();
            uId = user.get(SessionManager.keyId);
            uName = user.get(SessionManager.keyName);
          //  callProfileFragment();
        } else {
            //session.checkLogin();*/
        displayFragment(loginFragment);

    //}

        tvSignIn.setOnClickListener(v -> {
            tvSignIn.setTextColor(Color.parseColor("#E168F1"));
            tvSignUp.setTextColor(Color.parseColor("#60439b"));
            displayFragment(loginFragment);
        });

        tvSignUp.setOnClickListener(v -> {
            tvSignUp.setTextColor(Color.parseColor("#E168F1"));
            tvSignIn.setTextColor(Color.parseColor("#60439b"));
            displayFragment(signUpFragment);
        });

        fragmentList.add(loginFragment);
        fragmentList.add(signUpFragment);
        fragmentList.add(profileFragment);


    }

    public void callProfileFragment() {
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putString("userLoginId", uId);
        profileFragment.setArguments(bundle);


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment1, profileFragment);
        fragmentTransaction.hide(new LoginFragment());
        fragmentTransaction.hide(new SignUpFragment());
        fragmentTransaction.commit();
    }

    private int getFragmentIndex(Fragment fragment) {
        int index = -1;
        for (int i = 0; i < fragmentList.size(); i++) {
            if (fragment.hashCode() == fragmentList.get(i).hashCode()) {
                return i;
            }
        }
        return index;
    }

    private void displayFragment(Fragment fragment) {
        int index = getFragmentIndex(fragment);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (fragment.isAdded()) {
            fragmentTransaction.show(fragment);
        } else {
            fragmentTransaction.add(R.id.fragment1, fragment);
        }

        for (int i = 0; i < fragmentList.size(); i++) {
            if (fragmentList.get(i).isAdded() && i != index) {
                fragmentTransaction.hide(fragmentList.get(i));
            }
        }
        fragmentTransaction.commit();
    }
}