package com.example.androidtask;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import java.util.Objects;

public class SignUpFragment extends Fragment implements SignUpView, View.OnClickListener {

    EditText edFirstName, edLastName, edContact, edEmail, edPassword, edConfirmPassword;
    String firstname, lastname, contact, email, password, confirmpassword;
    Button btSignUp;
    com.example.androidtask.SignUpPresenter presenter;
    AwesomeValidation awesomeValidation;
    //ImageView ivProfile;
    public static final int RESULT_OK=-1;
    public static final int CAMERA_PERMISSION_CODE = 1;
    CheckBox cbAgree;

    public SignUpFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        // EditText....
        edFirstName = view.findViewById(R.id.edtFirstName);
        edLastName = view.findViewById(R.id.edtLastName);
        edContact = view.findViewById(R.id.edtContact);
        edEmail = view.findViewById(R.id.edtEmail);
        edPassword = view.findViewById(R.id.edtSPassword);
        edConfirmPassword = view.findViewById(R.id.edtSConfirmPassword);
        //ivProfile = view.findViewById(R.id.ivProfile);
        cbAgree=view.findViewById(R.id.cbAgree);

//        cbAgree.setButtonDrawable(R.drawable.gradient_button);

        btSignUp = view.findViewById(R.id.btnSignUp);


        presenter = new com.example.androidtask.SignUpPresenter(this);


      /*  ivProfile.setOnClickListener(v -> {
            selectImage();
        });*/

        btSignUp.setOnClickListener(this);


    }


    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallary", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setTitle("Add Photo!");
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);

            } else if (options[item].equals("Choose from Gallary")) {
                Intent gallaryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallaryIntent, 2);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    public void checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), permission) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new String[]{permission}, requestCode);
        } else {
            Toast.makeText(getContext(), "Permission already Granted", Toast.LENGTH_LONG).show();
            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(i, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Camera Permission Granted", Toast.LENGTH_LONG).show();
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i, 1);
            } else {
                Toast.makeText(getContext(), "Camera Permission Denied", Toast.LENGTH_LONG).show();
                showSettingsDialog();

            }
        }
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Need Permissions");
        builder.setMessage("This App needs permission to use this feature. You can grant them in App Settings");
        builder.setPositiveButton("GOTO Settings", ((dialog, which) -> {
            dialog.cancel();
            openSettings();
        }));
        builder.setNegativeButton("Cancel", ((dialog, which) -> {
            dialog.cancel();
        }));
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", "com.example.demoproject", null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if(requestCode==1){
                Bitmap photo=(Bitmap) data.getExtras().get("data");
             //   ivProfile.setImageBitmap(photo);
            }
            else if(requestCode==2){
                Uri selImage=data.getData();
            //    ivProfile.setImageURI(selImage);
            }
    }

    @Override
    public void putData(DataClassSignUp dc) {

    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void onClick(View v) {

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        awesomeValidation.addValidation(getActivity(), R.id.edtFirstName, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameError);
        awesomeValidation.addValidation(getActivity(), R.id.edtLastName, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameError);
        awesomeValidation.addValidation(getActivity(), R.id.edtContact, "^[0-9]{2}[0-9]{8}$", R.string.contactError);
        awesomeValidation.addValidation(getActivity(), R.id.edtEmail, Patterns.EMAIL_ADDRESS, R.string.emailError);
        awesomeValidation.addValidation(getActivity(), R.id.edtSPassword, "[0-9]{5}$", R.string.pswdError);
        awesomeValidation.addValidation(getActivity(), R.id.edtSConfirmPassword, "[0-9]{5}$", R.string.confirmPswdError);


        // Get Values....
        firstname = edFirstName.getText().toString();
        lastname = edLastName.getText().toString();
        contact = edContact.getText().toString();
        email = edEmail.getText().toString();
        password = edPassword.getText().toString();
        confirmpassword = edConfirmPassword.getText().toString();

        if (awesomeValidation.validate()) {


            if (password.equals(confirmpassword)) {

                //insert Data into Database

                presenter.putData(new DataClassSignUp(firstname, lastname, contact, email, password, v.getContext()));
                Toast.makeText(v.getContext(), "SignUp Successfully", Toast.LENGTH_LONG).show();


                // Replace SignUp Fragment to Login Fragment

                LoginFragment loginFragment = new LoginFragment();
                FragmentManager fragmentManager = getFragmentManager();
                assert fragmentManager != null;
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment1, loginFragment);

                fragmentTransaction.hide(this);
                fragmentTransaction.hide(new com.example.androidtask.ProfileFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            } else {
                Toast.makeText(v.getContext(), "Both Password are NOT Same! Try Again !", Toast.LENGTH_LONG).show();
            }
        }

    }
}