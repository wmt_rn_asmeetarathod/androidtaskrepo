package com.example.androidtask;

import android.content.Context;
import android.content.Intent;

public class LoginPresenter implements com.example.androidtask.LoginView {


    com.example.androidtask.LoginView view;

    public LoginPresenter(com.example.androidtask.LoginView view) {
        this.view = view;
    }

    @Override
    public String checkUser(String userName, String Pswd, Context context) {
        String response="0";
        com.example.androidtask.DBHelper helper=new com.example.androidtask.DBHelper(context);
        response=helper.checkUserData(userName,Pswd);
        return response;

    }



}
