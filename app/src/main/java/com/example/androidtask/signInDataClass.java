package com.example.androidtask;

import com.google.gson.annotations.SerializedName;

public class signInDataClass {

    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;

    @SerializedName("fcm_token")
    public String fcmToken;

    @SerializedName("device_type")
    public String deviceType;

    @SerializedName("device_name")
    public String deviceName;

    @SerializedName("os")
    public String os;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }
}
