package com.example.androidtask;

public interface SignUpView {

    void putData(com.example.androidtask.DataClassSignUp dc);

    void showLoader();

    void hideLoader();
}
