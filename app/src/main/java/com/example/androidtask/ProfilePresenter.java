package com.example.androidtask;

import android.content.Context;

public class ProfilePresenter implements com.example.androidtask.ProfileView {

    com.example.androidtask.ProfileView profileView;

    public ProfilePresenter(com.example.androidtask.ProfileView profileView) {
        this.profileView = profileView;
    }

    @Override
    public DataClassSignUp getUserData(String userId, Context context) {
        com.example.androidtask.DBHelper helper=new com.example.androidtask.DBHelper(context);
        DataClassSignUp dc=helper.getUserDataById(userId);
        return dc;
    }

    @Override
    public int updateData(DataClassSignUp dc) {
        int res=0;
        com.example.androidtask.DBHelper helper=new com.example.androidtask.DBHelper(dc.getContext());
        res=helper.updateUserProfile(dc);
        return res;


    }


}
