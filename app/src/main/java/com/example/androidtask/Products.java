package com.example.androidtask;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Products{

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("expiry_date")
	private int expiryDate;

	@SerializedName("description")
	private String description;

	@SerializedName("product_status")
	private int productStatus;

	@SerializedName("product_type_id")
	private int productTypeId;

	@SerializedName("terms_condition")
	private String termsCondition;

	@SerializedName("is_voucher")
	private int isVoucher;

	@SerializedName("uuid")
	private String uuid;

	@SerializedName("product_category_id")
	private int productCategoryId;

	@SerializedName("token_price")
	private int tokenPrice;

	@SerializedName("product_images")
	private List<ProductImagesItem> productImages;

	@SerializedName("product_type")
	private ProductType productType;

	@SerializedName("website_url")
	private Object websiteUrl;

	@SerializedName("vendor")
	private Vendor vendor;

	@SerializedName("vendor_id")
	private int vendorId;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("is_notify_all")
	private int isNotifyAll;

	@SerializedName("status")
	private int status;

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setExpiryDate(int expiryDate){
		this.expiryDate = expiryDate;
	}

	public int getExpiryDate(){
		return expiryDate;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setProductStatus(int productStatus){
		this.productStatus = productStatus;
	}

	public int getProductStatus(){
		return productStatus;
	}

	public void setProductTypeId(int productTypeId){
		this.productTypeId = productTypeId;
	}

	public int getProductTypeId(){
		return productTypeId;
	}

	public void setTermsCondition(String termsCondition){
		this.termsCondition = termsCondition;
	}

	public String getTermsCondition(){
		return termsCondition;
	}

	public void setIsVoucher(int isVoucher){
		this.isVoucher = isVoucher;
	}

	public int getIsVoucher(){
		return isVoucher;
	}

	public void setUuid(String uuid){
		this.uuid = uuid;
	}

	public String getUuid(){
		return uuid;
	}

	public void setProductCategoryId(int productCategoryId){
		this.productCategoryId = productCategoryId;
	}

	public int getProductCategoryId(){
		return productCategoryId;
	}

	public void setTokenPrice(int tokenPrice){
		this.tokenPrice = tokenPrice;
	}

	public int getTokenPrice(){
		return tokenPrice;
	}

	public void setProductImages(List<ProductImagesItem> productImages){
		this.productImages = productImages;
	}

	public List<ProductImagesItem> getProductImages(){
		return productImages;
	}

	public void setProductType(ProductType productType){
		this.productType = productType;
	}

	public ProductType getProductType(){
		return productType;
	}

	public void setWebsiteUrl(Object websiteUrl){
		this.websiteUrl = websiteUrl;
	}

	public Object getWebsiteUrl(){
		return websiteUrl;
	}

	public void setVendor(Vendor vendor){
		this.vendor = vendor;
	}

	public Vendor getVendor(){
		return vendor;
	}

	public void setVendorId(int vendorId){
		this.vendorId = vendorId;
	}

	public int getVendorId(){
		return vendorId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIsNotifyAll(int isNotifyAll){
		this.isNotifyAll = isNotifyAll;
	}

	public int getIsNotifyAll(){
		return isNotifyAll;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}
}