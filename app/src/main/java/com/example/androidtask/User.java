package com.example.androidtask;

import com.google.gson.annotations.SerializedName;

public class User{

	@SerializedName("birthdate")
	private String birthdate;

	@SerializedName("gender")
	private int gender;

	@SerializedName("mobile_no")
	private long mobileNo;

	@SerializedName("weight")
	private int weight;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("profile_picture")
	private String profilePicture;

	@SerializedName("uuid")
	private String uuid;

	@SerializedName("is_verified")
	private int isVerified;

	@SerializedName("token")
	private double token;

	@SerializedName("country_code")
	private String countryCode;

	@SerializedName("is_social")
	private int isSocial;

	@SerializedName("about_us")
	private String aboutUs;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("height")
	private int height;

	@SerializedName("total_token")
	private double totalToken;

	@SerializedName("status")
	private int status;

	public void setBirthdate(String birthdate){
		this.birthdate = birthdate;
	}

	public String getBirthdate(){
		return birthdate;
	}

	public void setGender(int gender){
		this.gender = gender;
	}

	public int getGender(){
		return gender;
	}

	public void setMobileNo(long mobileNo){
		this.mobileNo = mobileNo;
	}

	public long getMobileNo(){
		return mobileNo;
	}

	public void setWeight(int weight){
		this.weight = weight;
	}

	public int getWeight(){
		return weight;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setProfilePicture(String profilePicture){
		this.profilePicture = profilePicture;
	}

	public String getProfilePicture(){
		return profilePicture;
	}

	public void setUuid(String uuid){
		this.uuid = uuid;
	}

	public String getUuid(){
		return uuid;
	}

	public void setIsVerified(int isVerified){
		this.isVerified = isVerified;
	}

	public int getIsVerified(){
		return isVerified;
	}

	public void setToken(double token){
		this.token = token;
	}

	public double getToken(){
		return token;
	}

	public void setCountryCode(String countryCode){
		this.countryCode = countryCode;
	}

	public String getCountryCode(){
		return countryCode;
	}

	public void setIsSocial(int isSocial){
		this.isSocial = isSocial;
	}

	public int getIsSocial(){
		return isSocial;
	}

	public void setAboutUs(String aboutUs){
		this.aboutUs = aboutUs;
	}

	public String getAboutUs(){
		return aboutUs;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setHeight(int height){
		this.height = height;
	}

	public int getHeight(){
		return height;
	}

	public void setTotalToken(double totalToken){
		this.totalToken = totalToken;
	}

	public double getTotalToken(){
		return totalToken;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}
}