package com.example.androidtask;

import android.content.Context;

public interface ProfileView {

    public DataClassSignUp getUserData(String userId, Context context);

    public  int updateData(DataClassSignUp dc);

}
