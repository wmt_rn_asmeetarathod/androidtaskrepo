package com.example.androidtask;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("unread_notification_count")
	private int unreadNotificationCount;

	@SerializedName("notifications")
	private List<NotificationsItem> notifications;

	public void setUnreadNotificationCount(int unreadNotificationCount){
		this.unreadNotificationCount = unreadNotificationCount;
	}

	public int getUnreadNotificationCount(){
		return unreadNotificationCount;
	}

	public void setNotifications(List<NotificationsItem> notifications){
		this.notifications = notifications;
	}

	public List<NotificationsItem> getNotifications(){
		return notifications;
	}
}