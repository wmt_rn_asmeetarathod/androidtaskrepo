package com.example.androidtask;

import com.google.gson.annotations.SerializedName;

public class Challenges{

	@SerializedName("challenge_gift")
	private Object challengeGift;

	@SerializedName("distance_steps")
	private int distanceSteps;

	@SerializedName("description")
	private String description;

	@SerializedName("rules")
	private Object rules;

	@SerializedName("uuid")
	private String uuid;

	@SerializedName("challenge_type_id")
	private int challengeTypeId;

	@SerializedName("end_date_time")
	private int endDateTime;

	@SerializedName("challenge_fee")
	private Object challengeFee;

	@SerializedName("admin_id")
	private int adminId;

	@SerializedName("name")
	private String name;

	@SerializedName("tokens")
	private int tokens;

	@SerializedName("id")
	private int id;

	@SerializedName("is_complete")
	private int isComplete;

	@SerializedName("challenge_other_details")
	private ChallengeOtherDetails challengeOtherDetails;

	@SerializedName("start_date_time")
	private int startDateTime;

	@SerializedName("status")
	private int status;

	public void setChallengeGift(Object challengeGift){
		this.challengeGift = challengeGift;
	}

	public Object getChallengeGift(){
		return challengeGift;
	}

	public void setDistanceSteps(int distanceSteps){
		this.distanceSteps = distanceSteps;
	}

	public int getDistanceSteps(){
		return distanceSteps;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setRules(Object rules){
		this.rules = rules;
	}

	public Object getRules(){
		return rules;
	}

	public void setUuid(String uuid){
		this.uuid = uuid;
	}

	public String getUuid(){
		return uuid;
	}

	public void setChallengeTypeId(int challengeTypeId){
		this.challengeTypeId = challengeTypeId;
	}

	public int getChallengeTypeId(){
		return challengeTypeId;
	}

	public void setEndDateTime(int endDateTime){
		this.endDateTime = endDateTime;
	}

	public int getEndDateTime(){
		return endDateTime;
	}

	public void setChallengeFee(Object challengeFee){
		this.challengeFee = challengeFee;
	}

	public Object getChallengeFee(){
		return challengeFee;
	}

	public void setAdminId(int adminId){
		this.adminId = adminId;
	}

	public int getAdminId(){
		return adminId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setTokens(int tokens){
		this.tokens = tokens;
	}

	public int getTokens(){
		return tokens;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIsComplete(int isComplete){
		this.isComplete = isComplete;
	}

	public int getIsComplete(){
		return isComplete;
	}

	public void setChallengeOtherDetails(ChallengeOtherDetails challengeOtherDetails){
		this.challengeOtherDetails = challengeOtherDetails;
	}

	public ChallengeOtherDetails getChallengeOtherDetails(){
		return challengeOtherDetails;
	}

	public void setStartDateTime(int startDateTime){
		this.startDateTime = startDateTime;
	}

	public int getStartDateTime(){
		return startDateTime;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}
}