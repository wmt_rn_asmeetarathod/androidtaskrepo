package com.example.androidtask;

import com.google.gson.annotations.SerializedName;

public class ProductImagesItem{

	@SerializedName("product_id")
	private int productId;

	@SerializedName("id")
	private int id;

	@SerializedName("product_picture")
	private String productPicture;

	public void setProductId(int productId){
		this.productId = productId;
	}

	public int getProductId(){
		return productId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProductPicture(String productPicture){
		this.productPicture = productPicture;
	}

	public String getProductPicture(){
		return productPicture;
	}
}